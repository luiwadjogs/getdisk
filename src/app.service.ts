import { Injectable } from '@nestjs/common';
import { exec } from 'child_process';

function getAvailableSpace(path) {
  return new Promise((resolve, reject) => {
    const command = `df -h --output=avail /`;

    exec(command, (error, stdout, stderr) => {
      if (error) {
        reject(error);
      } else if (stderr) {
        reject(stderr);
      } else {
        const availableSpace = parseInt(stdout.trim().split('\n')[1]);
        resolve(availableSpace);
      }
    });
  });
}

@Injectable()
export class AppService {

  async getDisk(){
    return await getAvailableSpace('/');
 }

}
