module.exports = {
    apps : [
        {
            name: "GET-DISK",
            script: "./dist/main.js",
            watch: true,
            env: {
                "NODE_ENV": "production",
            }
        }
    ]
}